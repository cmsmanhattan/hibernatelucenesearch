package hibersearch.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import hibersearch.hibersearch.RequestForm;
import hibersearch.hibersearch.User;


@RestController
public class SearchController {
    /**
     *
     * @param name the name to greet
     * @return greeting text
     */
    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public String greetingText(@PathVariable String name) {
        return "Hello " + name + "!";
    }
    
    
	@PostMapping(value = "/doGetSearch",produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody User doGetSearch(@RequestBody RequestForm requestForm)
			throws Exception {
		
		String sortField = requestForm.getSortField() ;
		// call DAO layer .
		// it is not done yet .
		// See class IndexAndSearchTest because it is done there 
		User user = new User();
		user.setAddress1("11");
		user.setName("Grabko");
		user.setAge(48);
	    return user;
	}
}
