package hibersearch.hibersearch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

//import org.apache.lucene.search.Query;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Example testcase for Hibernate Search
 */
public class IndexAndSearchTest {

	private EntityManagerFactory emf;

	private EntityManager em;

	private static Logger log = LoggerFactory.getLogger(IndexAndSearchTest.class);

	@Before
	public void setUp() {
		initHibernate();
	}

	@After
	public void tearDown() {
		purge();
	}

	// it works but out of task scope
	// @Test
	public void testIndexAndSearchInALL() throws Exception {
		long time = System.currentTimeMillis();
		List<User> users = searchInAll("Grabko", 10);
		time = System.currentTimeMillis() - time;
		assertTrue("the query must be less 1 sec ", time < 1000);
		assertEquals("Should find one user", 1, users.size());
		assertEquals("Wrong user", "Grabko", users.get(0).getName());
	}

	@Test
	public void testIndexAndSearchByNameAndAgeNoOk1() throws Exception {
		long time = System.currentTimeMillis();
		List<User> users = searchByNameAndAge("Grabko", 49);
		time = System.currentTimeMillis() - time;
		assertTrue("the query must be less 1 sec ", time < 1000);
		assertEquals("Should find zero users", 0, users.size());

	}

	@Test
	public void testIndexAndSearchByNameAndAgeNoOk2() throws Exception {
		long time = System.currentTimeMillis();
		List<User> users = searchByNameAndAge("Smit", 48);
		time = System.currentTimeMillis() - time;
		assertTrue("the query must be less 1 sec ", time < 1000);
		assertEquals("Should find zero users", 0, users.size());
	}

	@Test
	public void testIndexAndSearchByNameAndAgeOK() throws Exception {
		long time = System.currentTimeMillis();
		List<User> users = searchByNameAndAge("Grabko", 48);
		time = System.currentTimeMillis() - time;
		assertTrue("the query must be less 1 sec ", time < 1000);
		assertEquals("Should find one user", 1, users.size());
		assertEquals("Correct user", "Grabko", users.get(0).getName());
	}

	@Test
	public void testIndexAndSearchByNameAndAgeAndSortByAgeOK() throws Exception {
		long time = System.currentTimeMillis();
		Sort sort = new Sort(new SortField[] { SortField.FIELD_SCORE, new SortField("age", SortField.INT) });

		List<User> users = searchInALL("Grabko", sort);
		time = System.currentTimeMillis() - time;
		assertTrue("the query must be less 1 sec ", time < 1000);
		assertTrue("user age ", 48 == users.get(0).getAge());
		assertEquals("Second Grabko", "Grabko", users.get(0).getName());

		assertTrue("user age ", 80 == users.get(1).getAge());
		assertEquals("First Grabko", "Grabko", users.get(1).getName());
	}

	private void initHibernate() {
		int maxDbRecords = 10000; // 1000 000 - put 1M here for test . it take too much time to fill db
		emf = Persistence.createEntityManagerFactory("hibernate-search-example");
		em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			int BATCH_SIZE = 10000;
			for (int i = 0; i < maxDbRecords; i++) {
				if (i > 0 && i % BATCH_SIZE == 0) {
					em.flush();
					em.clear();
					tx.commit();
					tx.begin();
				}
				User user = new User();
				user.setAddress1("Job adress" + i);
				user.setAddress2("Home adress" + i);
				user.setAge(i);
				user.setName("Name" + i);
				em.persist(user);
			}

			User user = new User();
			user.setAddress1("33 Mitchell ave Plainview NY");
			user.setAddress2("86 Sunnyside blvd Plainview NY");
			user.setAge(80);
			user.setName("Grabko");
			em.persist(user);

			User user2 = new User();
			user2.setAddress1("33 Mitchell ave Plainview NY");
			user2.setAddress2("86 Sunnyside blvd Plainview NY");
			user2.setAge(48);
			user2.setName("Grabko");
			em.persist(user2);

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}

		index();
		// List<User> users = em.createQuery("from User").getResultList();
		// System.out.println("Users count: " + users.size());
	}

	private void index() {
		FullTextEntityManager ftEm = org.hibernate.search.jpa.Search.getFullTextEntityManager(em);
		try {
			ftEm.createIndexer().startAndWait();
		} catch (InterruptedException e) {
			log.error("Was interrupted during indexing", e);
		}
	}

	private void purge() {
		FullTextEntityManager ftEm = org.hibernate.search.jpa.Search.getFullTextEntityManager(em);

		ftEm.purgeAll(User.class);
		ftEm.flushToIndexes();
		ftEm.close();
		emf.close();
	}

	private List<User> searchInAll(String searchQuery, int limit) throws ParseException {
		Query query = searchQueryInALL(searchQuery);
		query.setMaxResults(limit);
		List<User> users = query.getResultList();

		for (User b : users) {
			log.info("Name: " + b.getName());
		}
		return users;
	}

	private Query searchQueryInALL(String searchQuery) throws ParseException {

		String[] userFields = { "name", "age", "address1", "address2" };

		// lucene part
		Map<String, Float> boostPerField = new HashMap<String, Float>(4);
		boostPerField.put(userFields[0], (float) 4);
		boostPerField.put(userFields[1], (float) 3);
		boostPerField.put(userFields[2], (float) 4);
		boostPerField.put(userFields[3], (float) .5);

		FullTextEntityManager ftEm = org.hibernate.search.jpa.Search.getFullTextEntityManager(em);
		Analyzer customAnalyzer2 = ftEm.getSearchFactory().getAnalyzer("customanalyzer1");
		QueryParser parser = new MultiFieldQueryParser(Version.LUCENE_34, userFields, customAnalyzer2, boostPerField);

		org.apache.lucene.search.Query luceneQuery;
		luceneQuery = parser.parse(searchQuery);

		final FullTextQuery query = ftEm.createFullTextQuery(luceneQuery, User.class);

		return query;
	}

	private Query searchQueryInALL(Sort sort) throws ParseException {

		String[] userFields = { "name", "age", "address1", "address2" };

		// lucene part
		Map<String, Float> boostPerField = new HashMap<String, Float>(4);
		boostPerField.put(userFields[0], (float) 4);
		boostPerField.put(userFields[1], (float) 3);
		boostPerField.put(userFields[2], (float) 4);
		boostPerField.put(userFields[3], (float) .5);

		FullTextEntityManager ftEm = org.hibernate.search.jpa.Search.getFullTextEntityManager(em);
		Analyzer customAnalyzer2 = ftEm.getSearchFactory().getAnalyzer("customanalyzer1");
		QueryParser parser = new MultiFieldQueryParser(Version.LUCENE_34, userFields, customAnalyzer2, boostPerField);

		org.apache.lucene.search.Query luceneQuery;
		luceneQuery = parser.parse(QueryParser.escape("name:*"));

		final FullTextQuery query = ftEm.createFullTextQuery(luceneQuery, User.class);
		query.setSort(sort);

		return query;
	}

	private List<User> searchByNameAndAge(String name, Integer age) throws ParseException {
		Query query = searchQueryByNameAndAge(name, age);

		List<User> users = query.getResultList();

		for (User b : users) {
			log.info("Name: " + b.getName());
		}
		return users;
	}

	/**
	 * 
	 * @param name
	 * @param age
	 * @param sort new Sort(new SortField[] { SortField.FIELD_SCORE, new
	 *             SortField("field_1", SortField.STRING), new SortField("field_2",
	 *             SortField.STRING) });
	 * @return
	 * @throws ParseException
	 */
	private List<User> searchByNameAndAge(String name, Integer age, Sort sort) throws ParseException {
		Query query = searchQueryByNameAndAge(name, age, sort);

		List<User> users = query.getResultList();

		for (User b : users) {
			log.info("Name: " + b.getName());
		}
		return users;
	}

	private List<User> searchInALL(String name, Sort sort) throws ParseException {
		Query query = searchQueryByAge(name, sort);
		List<User> users = query.getResultList();

		for (User b : users) {
			log.info("Name: " + b.getName());
		}
		return users;
	}

	private Query searchQueryByNameAndAge(String name, Integer age) throws ParseException {

		FullTextEntityManager ftEm = org.hibernate.search.jpa.Search.getFullTextEntityManager(em);

		QueryBuilder userQB = ftEm.getSearchFactory().buildQueryBuilder().forEntity(User.class).get();
		org.apache.lucene.search.Query luceneQuery = userQB.bool()
				.must(userQB.keyword().onField("name").matching(name).createQuery())
				.must(userQB.keyword().onField("age").matching(age).createQuery()).createQuery();

		final FullTextQuery query = ftEm.createFullTextQuery(luceneQuery, User.class);

		return query;
	}

	private Query searchQueryByNameAndAgeRange(String name, Integer age) throws ParseException {

		FullTextEntityManager ftEm = org.hibernate.search.jpa.Search.getFullTextEntityManager(em);

		QueryBuilder userQB = ftEm.getSearchFactory().buildQueryBuilder().forEntity(User.class).get();
		org.apache.lucene.search.Query luceneQuery = userQB.bool()
				.must(userQB.keyword().onField("name").matching(name).createQuery())
				.must(userQB.range().onField("age").above(age).createQuery()).createQuery();

		final FullTextQuery query = ftEm.createFullTextQuery(luceneQuery, User.class);

		return query;
	}

	private Query searchQueryByNameAndAge(String name, Integer age, Sort sort) throws ParseException {

		FullTextEntityManager ftEm = org.hibernate.search.jpa.Search.getFullTextEntityManager(em);

		QueryBuilder userQB = ftEm.getSearchFactory().buildQueryBuilder().forEntity(User.class).get();
		org.apache.lucene.search.Query luceneQuery = userQB.bool()
				.must(userQB.keyword().onField("name").matching(name).createQuery())
				.must(userQB.range().onField("age").above(age).createQuery()).createQuery();

		final FullTextQuery query = ftEm.createFullTextQuery(luceneQuery, User.class);
		query.setSort(sort);
		return query;
	}

	private Query searchQueryByAge(Integer age, Sort sort) throws ParseException {

		FullTextEntityManager ftEm = org.hibernate.search.jpa.Search.getFullTextEntityManager(em);

		QueryBuilder userQB = ftEm.getSearchFactory().buildQueryBuilder().forEntity(User.class).get();
		org.apache.lucene.search.Query luceneQuery = userQB.bool()
				.must(userQB.range().onField("age").above(age).createQuery()).createQuery();

		final FullTextQuery query = ftEm.createFullTextQuery(luceneQuery, User.class);
		query.setSort(sort);
		return query;
	}

	private Query searchQueryByAge(String name, Sort sort) throws ParseException {

		FullTextEntityManager ftEm = org.hibernate.search.jpa.Search.getFullTextEntityManager(em);

		QueryBuilder userQB = ftEm.getSearchFactory().buildQueryBuilder().forEntity(User.class).get();
		org.apache.lucene.search.Query luceneQuery = userQB.bool()
				.must(userQB.keyword().onField("name").matching(name).createQuery()).createQuery();

		final FullTextQuery query = ftEm.createFullTextQuery(luceneQuery, User.class);
		query.setSort(sort);
		return query;
	}

}
