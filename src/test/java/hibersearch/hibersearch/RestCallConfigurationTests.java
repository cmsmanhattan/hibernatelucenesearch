/*
 * Copyright 2012-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	  https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Source https://www.baeldung.com/rest-template
 */
package hibersearch.hibersearch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.net.HttpURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;



@SuppressWarnings("unused")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class RestCallConfigurationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testGreeting() throws Exception {
		ResponseEntity<String> entity = restTemplate.getForEntity("http://localhost:" + this.port + "/Java",
				String.class);
		assertEquals(HttpStatus.OK, entity.getStatusCode());
	}

	
	/**
	 * I did not have time to complete it So check the class for business logic  IndexAndSearchTest.
	 * It works with lucene 
	 * @throws Exception
	 */
	
	@Test
	public void testDoGetSearch() throws Exception {

		RequestForm form = new RequestForm();
		form.setAge(48);
		form.setName("Grabko");
		form.setSortField("age");

		HttpEntity<RequestForm> requestForm = new HttpEntity<RequestForm>(form);
		
		ResponseEntity<User> userResponse = restTemplate
				.postForEntity("http://localhost:" + this.port + "/doGetSearch", requestForm, User.class);

		Assert.assertEquals(200, userResponse.getStatusCodeValue());
		User reponseDTO = userResponse.getBody();
		assertNotNull(reponseDTO);
		// fix it
	    assertEquals(reponseDTO.getName(), "Grabko");
		assertTrue(true);

	}
}
